<?php

include('php/contactData.php');
require('php/process.php');

$db_con = new MySqlDrive();
$query = "SELECT * FROM info";
	            // $result = $db_con->read($query);
$result = mysqli_query($db_con->db_connect(), $query);
	            // var_dump($result);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | B&Y</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
	<div>
		<div class="container">
			<div class="navbar">
				<div class="logo">
					<a href="index.php"><img src="images/by-logo.png" width="75px"></a>
				</div>
				<nav>
					<ul id="MenuItems">
						<li><a href="index.php">Home</a></li>
						<li><a href="products.php">Products</a></li>
						<li><a href="about.php">About Us</a></li>
						<li><a href="contact.php">Contact</a></li>
						<li><a href="account.php">Account</a></li>
						<?php
						if (isset($_SESSION['roli']) && $_SESSION['roli']==1) {
							?>
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="php/logout.php">Logout</a></li>
							<?php
						} 
						?>
						<?php
						if (isset($_SESSION['roli']) && $_SESSION['roli']==0) {
							?>
							<li><a href="php/logout.php">Logout</a></li>
							<?php
						} 
						?>
					</ul>
				</nav>
				<a href="cart.html"><img src="images/cart.png" width="30px" height="30px"></a>
				<img src="images/menu.png" class="menu-icon" onclick="menutoggle()">
			</div>
		</div>
	</div>
	<!--------------------------- contact Data ------------------------->
	<div class="small-container">
		<h2 class="title">Contact Form Results</h2>
		<form method="POST">
			<input type="submit" name="button1"
			class="btn" value="Show Contact Form Results" >
		</form>
		<?php
		if(isset($_POST['button1'])) {
			if ($result1->num_rows > 0) {
				echo "<table><tr><th>ID</th><th>Name</th><th>Email</th><th>Phone</th><th>Message</th></tr>";
    // output data of each row
				while($row1 = $result1->fetch_assoc()) {
					echo "<tr><td>".$row1["Id"]."</td><td>".$row1["fldName"]."</td><td>".$row1["fldEmail"]."</td><td>".$row1["fldPhone"]."</td><td>".$row1["fldMessage"]."</td></tr>";
				}
				echo "</table>";
			} else {
				echo "0 results";
			}
			$conn1->close();
		}
		?>
	</div>
	<br>
	<br>

	<!-------------------Products ------------------------->
	<div class="small-container">
		<h2 class="title">Product Database</h2>
		
		<?php //$results = mysqli_query($db, "SELECT * FROM info"); ?>
		
		<?php if(isset($_SESSION['message'])): ?>
			
			<h3>
				<?php echo $_SESSION['message']; ?>
			</h3>
		<?php endif ?>
		
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Image</th>
					<th>Description</th>
					<th>Price</th>
					<th>Details</th>
					<th colspan="2">Action</th>
				</tr>
			</thead>
			<?php while($row = $result->fetch_assoc()){ ?>
				<tr>
					<td><?php echo $row['id']; ?></td>
					<td><?php echo $row['pImage']; ?></td>
					<td><?php echo $row['pDesc']; ?></td>
					<td><?php echo $row['pPrice']; ?></td>
					<td><?php echo $row['pDetails']; ?></td>
					<td>
						<a href="dashboard.php?edit=<?php echo $row['id']; ?>" class="btn" >Edit</a>
					</td>
					<td>
						<a href="php/process.php?delete=<?php echo $row['id']; ?>" class="btn">Delete</a>
					</td>
				</tr>
			<?php } ?>
		</table>

		<h3>Insert Data</h3>
		<form method="post" action="php/process.php" >
			
			<label>Product Image</label>
			<input type="file" name="image" value="<?php echo $image; ?>">

			<label>Product Description</label>
			<input type="text" name="desc" value="<?php echo $desc; ?>">

			<label>Product Price</label>
			<input type="text" name="price" value="<?php echo $price; ?>">

			<label>Product Details</label>
			<input type="text" name="details" value=<?php echo $details; ?>"">

			<input type="hidden" name="id" value="<?php echo $id; ?>">

			<?php if ($update == true): ?>
				<button class="btn" type="submit" name="update">Update</button>
			<?php else: ?>
				<button class="btn" type="submit" name="save" >Save</button>
			<?php endif ?>
		</form>
	</div>

	<!--------------------------- Footer ------------------------->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="footer-col-1">
					<h3>Download Our App</h3>
					<p>Download App for Android and iOS mobile phone</p>
					<div class="app-logo">
						<img src="images/play-store.png">
						<img src="images/app-store.png">
					</div>
				</div>
				<div class="footer-col-2">
					<img src="images/by-logo-white.png">
					<p>Our Purpose Is To Sustainably Make the Pleasure and Benefits of Sports Accessible to the Many. </p>
				</div>
				<div class="footer-col-3">
					<h3>Useful Links</h3>
					<ul>
						<li>Coupons</li>
						<li>Blog Post</li>
						<li>Return Policy</li>
						<li>Join Affiliate</li>
					</ul>
				</div>
				<div class="footer-col-4">
					<h3>Follow Us</h3>
					<ul>
						<li>Facebook</li>
						<li>Twitter</li>
						<li>Instagram</li>
						<li>YouTube</li>
					</ul>
				</div>

			</div>
			<hr><p class="copyright">Copyright 2021 B&Y Holding</p>
		</div>
	</div>

	<!---------------------JavaScript per Menu toggle--------------------->
	<script>
		var MenuItems = document.getElementById('MenuItems');
		MenuItems.style.maxHeight = '0px';
		function menutoggle() {
			if(MenuItems.style.maxHeight == '0px'){
				MenuItems.style.maxHeight = '300px';
			} else{
				MenuItems.style.maxHeight = '0px';
			}
		}
	</script>


</body>
</html>