<?php  
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>B&Y | Your favorite E-shop</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="navbar">
				<div class="logo">
					<a href="index.html"><img src="images/by-logo.png" width="75px"></a>
				</div>
				<nav>
					<ul id="MenuItems">
						<li><a href="index.php">Home</a></li>
						<li><a href="products.php">Products</a></li>
						<li><a href="about.php">About Us</a></li>
						<li><a href="contact.php">Contact</a></li>
						<li><a href="account.php">Account</a></li>
						<?php
						if (isset($_SESSION['roli']) && $_SESSION['roli']==1) {
							?>
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="php/logout.php">Logout</a></li>
							<?php
						} 
						?>
						<?php
						if (isset($_SESSION['roli']) && $_SESSION['roli']==0) {
							?>
							<li><a href="php/logout.php">Logout</a></li>
							<?php
						} 
						?>
						

					</ul>
				</nav>
				<a href="cart.html"><img src="images/cart.png" width="30px" height="30px"></a>
				<img src="images/menu.png" class="menu-icon" onclick="menutoggle()">
			</div>
			<div class="row">
				<div class="col-2">
					<h1>Give your Workout<br> A New Style!</h1>
					<p>Success isn't always about greatness. It's about consistency. Consistent <br>hard work gains success. Greatness will come.</p>
					<a href="" class="btn">Explore Now &#8594;</a>
				</div>
				<div class="col-2">
					<img src="images/image1.png">
				</div>
			</div>
		</div>
	</div>
	<!-------------------- Featured Categories ------------------->
	<div class="categories">
		<div class="small-container">
			<div class="row">
				<div class="col-3">
					<a href="products-detail.php?product=8">
						<img src="images/category-1.jpg">
					</a>
				</div>
				<div class="col-3">
					<a href="products-detail.php?product=9">
						<img src="images/category-2.jpg">
					</a>
				</div>
				<div class="col-3">
					<a href="products-detail.php?product=6">
						<img src="images/category-3.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-------------------- Featured Products ------------------->
	<div class="small-container">
		<h2 class="title">Featured Products</h2>
		<div class="row">
			<div class="col-4">
				<a href="products-detail.php?product=10">
					<img src="images/product-1.jpg">
				</a>
				<h4>Red Printed Tshirt</h4>
				<div class="rating">
					<span class="star">&#9733;&#9733;&#9733;&#9733;&#9734;</p>
					</div>
					<p>$50.00</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=5">
						<img src="images/product-2.jpg">
					</a>
					<h4>Black Shoes by GLK</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9734;&#9734;</span>
					</div>
					<p>$35.00</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=4">
						<img src="images/product-3.jpg">
					</a>
					<h4>Gray Sweatpants</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
					</div>
					<p>$25.00</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=11">
						<img src="images/product-4.jpg">
					</a>
					<h4>Blue Puma Shirt</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9734;&#9734;&#9734;</span>
					</div>
					<p>$40.00</p>
				</div>
			</div>
			<h2 class="title">Latest Products</h2>
			<div class="row">
				<div class="col-4">
					<a href="products-detail.php?product=12">
						<img src="images/product-5.jpg">
					</a>
					<h4>Gray Shoes</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9733;&#9734;</span>
					</div>
					<p>$50.00</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=13">
						<img src="images/product-6.jpg">
					</a>
					<h4>Black Puma Shirt</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9734;&#9734;</span>
					</div>
					<p>$42.00</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=14">
						<img src="images/product-7.jpg">
					</a>
					<h4>White/Black Socks</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
					</div>
					<p>$14.99</p>
				</div>
				<div class="col-4">
					<a href="products-detail.php?product=15">
						<img src="images/product-8.jpg">
					</a>
					<h4>Fossil Classic Watch</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9734;&#9734;&#9734;</span>
					</div>
					<p>$120.00</p>
				</div>
			</div>
			<div class="row">
				<div class="col-4">
					<a href="products-detail.php?product=16">
						<img src="images/product-9.jpg">
					</a>
					<h4>Fossil Sports Watch</h4>
					<div class="rating">
						<span class="star">&#9733;&#9733;&#9733;&#9733;&#9734;</p>
						</div>
						<p>$100.00</p>
					</div>
					<div class="col-4">
						<a href="products-detail.php?product=17">
							<img src="images/product-10.jpg">
						</a>
						<h4>Black Sport Shoes</h4>
						<div class="rating">
							<span class="star">&#9733;&#9733;&#9733;&#9734;&#9734;</span>
						</div>
						<p>$74.99</p>
					</div>
					<div class="col-4">
						<a href="products-detail.php?product=18">
							<img src="images/product-11.jpg">
						</a>
						<h4>Gray Walking Shoes</h4>
						<div class="rating">
							<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
						</div>
						<p>$55.29</p>
					</div>
					<div class="col-4">
						<a href="products-detail.php?product=19">
							<img src="images/product-12.jpg">
						</a>
						<h4>Black Sweatpants</h4>
						<div class="rating">
							<span class="star">&#9733;&#9733;&#9734;&#9734;&#9734;</span>
						</div>
						<p>$44.99</p>
					</div>
				</div>
			</div>
			<!--------------------  Offer ------------------->
			<div class="offer">
				<div class="small-container">
					<div class="row">
						<div class="col-2">
							<img src="images/exclusive1.png" class="offer-img">
						</div>
						<div class="col-2">
							<p>Exclusively Available on B&Y</p>
							<h1>Off White x Nike Air Force 1</h1>
							<small>The Off White x Nike Air Force 1 is the latest release of the Off White x Nike collab featuring the iconic Nike Air Force 1 sneakers with the white laces.</small><br>
							<a href="products-detail.php?product=20" class="btn">Buy Now &#8594;</a>
						</div>
					</div>
				</div>
			</div>
			<!-----------------------------------Testimonials-------------------------------- -->
			<div class="testimonial">
				<div class="small-container">
					<div class="row">
						<div class="col-3">
							<p><b>&#8220;</b> Beatiful shopping experience for almost every aspect you look for <b>&#8221;</b></p>
							<div class="rating">
								<span class="star">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
							</div>
							<img src="images/user-1.png">
							<h3>Mimosa Verda</h3>
						</div>
						<div class="col-3">
							<p><b>&#8220;</b> Beatiful shopping experience for almost every aspect you look for <b>&#8221;</b></p>
							<div class="rating">
								<span class="star">&#9733;&#9733;&#9733;&#9733;&#9734;</span>
							</div>
							<img src="images/user-2.png">
							<h3>Sean Parker</h3>
						</div>
						<div class="col-3">
							<p><b>&#8220;</b> Beatiful shopping experience for almost every aspect you look for <b>&#8221;</b></p>
							<div class="rating">
								<span class="star">&#9733;&#9733;&#9734;&#9734;&#9734;</span>
							</div>
							<img src="images/user-3.png">
							<h3>Celina Holland</h3>
						</div>
					</div>
				</div>
			</div>
			<!-----------------------------Brands ------------------------->
			<div class="brands">
				<div class="small-container">
					<div class="row">
						<div class="col-5">
							<img src="images/logo-nike.png">
						</div>
						<div class="col-5">
							<img src="images/logo-adidas.png">
						</div>
						<div class="col-5">
							<img src="images/logo-coca-cola.png">
						</div>
						<div class="col-5">
							<img src="images/logo-paypal.png">
						</div>
						<div class="col-5">
							<img src="images/logo-western.png">
						</div>
					</div>
				</div>
			</div>

			<!---------------------------Footer------------------------->
			<div class="footer">
				<div class="container">
					<div class="row">
						<div class="footer-col-1">
							<h3>Download Our App</h3>
							<p>Download App for Android and iOS mobile phone</p>
							<div class="app-logo">
								<img src="images/play-store.png">
								<img src="images/app-store.png">
							</div>
						</div>
						<div class="footer-col-2">
							<img src="images/by-logo-white.png">
							<p>Our Purpose Is To Sustainably Make the Pleasure and Benefits of Sports Accessible to the Many. </p>
						</div>
						<div class="footer-col-3">
							<h3>Useful Links</h3>
							<ul>
								<li>Coupons</li>
								<li>Blog Post</li>
								<li>Return Policy</li>
								<li>Join Affiliate</li>
							</ul>
						</div>
						<div class="footer-col-4">
							<h3>Follow Us</h3>
							<ul>
								<li>Facebook</li>
								<li>Twitter</li>
								<li>Instagram</li>
								<li>YouTube</li>
							</ul>
						</div>

					</div>
					<hr><p class="copyright">Copyright 2021 B&Y Holding</p>
				</div>
			</div>

			<!---------------------JavaScript per Menu toggle--------------------->
			<script>
				var MenuItems = document.getElementById('MenuItems');
				MenuItems.style.maxHeight = '0';
				function menutoggle() {
					if(MenuItems.style.maxHeight == '0px'){
						MenuItems.style.maxHeight = '300px';
					} else{
						MenuItems.style.maxHeight = '0px';
					}
				}
			</script>
		</body>
		</html>